CREATE TABLE Usuario (
id_usuario 		INT NOT NULL AUTO_INCREMENT,
nome 			VARCHAR(50),
senha 			VARCHAR(50),
email 			VARCHAR(50),
PRIMARY KEY		(id_usuario)
);

CREATE TABLE PayPal (
id_paypal 		INT NOT NULL AUTO_INCREMENT,
id_usuario 		INT NOT NULL,
usuario 		VARCHAR(50),
senha 			VARCHAR(50),
PRIMARY KEY		(id_paypal),
FOREIGN KEY		(id_usuario) REFERENCES Usuario (id_usuario)
);

CREATE TABLE Cartao (
id_cartao 		INT NOT NULL AUTO_INCREMENT,
id_usuario 		INT NOT NULL,
nome			VARCHAR(50),
numero 			VARCHAR(50),
bandeira 		VARCHAR(15),
vencimento 		VARCHAR(6),
PRIMARY KEY		(id_cartao),
FOREIGN KEY(id_usuario) REFERENCES Usuario (id_usuario)
);

CREATE TABLE Pagamentos (
id_pagamento 	INT NOT NULL AUTO_INCREMENT,
id_usuario 		INT NOT NULL,
data 			DATETIME,
valor 			VARCHAR(50),
tipo 			VARCHAR(50),
PRIMARY KEY		(id_pagamento),
FOREIGN KEY		(id_usuario) REFERENCES Usuario (id_usuario)
);


INSERT INTO Usuario (nome, senha, email) VALUES ('Joao', '123', 'joao@joao.com');
INSERT INTO Usuario (nome, senha, email) VALUES ('Pedro', '1234', 'pedro@pedro.com');
INSERT INTO Usuario (nome, senha, email) VALUES ('Roberto', '12345', 'roberto@roberto.com');

INSERT INTO PayPal (id_usuario, usuario, senha) VALUES (1, 'joao@joao.com', '123');

INSERT INTO Cartao (id_usuario, nome, numero, bandeira, vencimento) VALUES (1, 'Joao', '4444', 'visa', '052020');


/*
db			jdbc:mysql://yonathan.com.br/prog-web-1
login		faculdades
passwd		FbZRwUZDBXNZP7Ty
*/
