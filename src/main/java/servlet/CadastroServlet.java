package servlet;

import controle.ClienteDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.Cliente;

/**
 *
 * @author 0230090
 */
@WebServlet(name = "CadastroServlet", urlPatterns = {"/cadastro"})
public class CadastroServlet extends HttpServlet {

    private final String url = "jdbc:mysql://yonathan.com.br/prog-web-1";
    private final String login = "faculdades";
    private final String passwd = "FbZRwUZDBXNZP7Ty";
    private Connection con;
    private ClienteDAO cdao;

    @Override
    public void init(ServletConfig config) throws ServletException {
        try {
            super.init(config);
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, login, passwd);
        } catch (ClassNotFoundException | SQLException ex) {

        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        init(this);         
        String result = "erro";
        request.setAttribute("resultado", result);

        try {
            String vNome = request.getParameter("nome");
//            String vRg = request.getParameter("rg");
//            String vCpf = request.getParameter("cpf");
//            String vTel = request.getParameter("telefone");
//            String vEnd = request.getParameter("endereco");
            String vEmail = request.getParameter("email");
            String vSen = request.getParameter("senha");

            Cliente cliente = new Cliente();
            cliente.setNome(vNome);
            cliente.setEmail(vEmail);
            cliente.setSenha(vSen);

            //  new ClienteDAO().criar(con, cliente);
            
            cdao = new ClienteDAO();
            cdao.criar(con, cliente);
//            con.commit();

            request.setAttribute("user", cliente);
            
            Cliente resp = cdao.pesquisar(con, cliente);  
            
                Cookie cookie = new Cookie("identifica", resp.getNome());
                Long i=resp.getCodCliente();
                Cookie cookie2 = new Cookie("id", i.toString() );
                response.addCookie(cookie);
                response.addCookie(cookie2);
                 
                HttpSession session = request.getSession(true);
                session.setAttribute("identifica", resp.getNome());
                session.setAttribute("email", resp.getEmail());
                session.setAttribute("id", resp.getCodCliente());

        } catch (Exception e) {
            request.getRequestDispatcher("erro.jsp");
        }

        response.setContentType("text/html;charset=UTF-8");
     
        request.getRequestDispatcher("cadastrarpagamento.jsp").forward(request, response);

//        response.sendRedirect("resposta.jsp");
    }

}
