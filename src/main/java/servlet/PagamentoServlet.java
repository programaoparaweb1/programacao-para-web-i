package servlet;

import controle.CartaoDAO;
import controle.ClienteDAO;
import controle.InterfaceBanco;
import controle.PaypalDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.Cartao;
import modelo.Paypal;

/**
 *
 * @author 0230090
 */
@WebServlet(name = "PagamentoServlet", urlPatterns = {"/pagamento"})
public class PagamentoServlet extends HttpServlet {

    private final String url = "jdbc:mysql://yonathan.com.br/prog-web-1";
    private final String login = "faculdades";
    private final String passwd = "FbZRwUZDBXNZP7Ty";
    private Connection con;
    private InterfaceBanco ccdao;

    @Override
    public void init(ServletConfig config) throws ServletException {
        try {
            super.init(config);
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, login, passwd);
        } catch (ClassNotFoundException | SQLException ex) {

        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        init(this);
        String vTipo = request.getParameter("tipo");
        switch (vTipo) {
            case "paypal":
                {
                    String vLogin = request.getParameter("login");
                    String vPass = request.getParameter("pass");
                    HttpSession session = request.getSession(true);
                    Object attribute = session.getAttribute("id");
                    String idStr = attribute.toString();
                    int id = Integer.parseInt(idStr);
                    Paypal pay = new Paypal();
                    pay.setIdCliente(id);
                    pay.setSenha(vPass);
                    pay.setUsuario(vLogin);
                    ccdao = new PaypalDAO();
                    try {
                        ccdao.criar(con, pay);
                    } catch (SQLException ex) {
                        Logger.getLogger(PagamentoServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }       break;
                }
            case "cartao":
                {
                    String vNome = request.getParameter("nome");
                    String vNum = request.getParameter("numero");
                    String vDate = request.getParameter("date");
                    String vBand = request.getParameter("band");
                    int num = Integer.parseInt(vNum);
                    Date date = null;
//            try {
//                DateFormat formatter = new SimpleDateFormat("dd/mm/yyyy");
//                date = (java.util.Date) formatter.parse(vDate);
//            } catch (ParseException e) {
//               System.out.println("Erro de data");
//            }
                    HttpSession session = request.getSession(true);
                    Object attribute = session.getAttribute("id");
                    String javaehumabosta = attribute.toString();
                    int id = Integer.parseInt(javaehumabosta);
                    //.setAttribute("identifica", resp.getNome());
                    Cartao card = new Cartao();
                    card.setNome(vNome);
                    card.setBandeira(vBand);
                    card.setNumero(num);
                    card.setVencimento(date);
                    card.setId_usuario(id);
                    ccdao = new CartaoDAO();
                    try {
                        ccdao.criar(con, card);
//                con.commit();
                    } catch (SQLException ex) {
                        Logger.getLogger(PagamentoServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }       break;
                }
            default:
                break;
        }
        request.setAttribute("resultado", vTipo);
        //resposta.jsp
        request.getRequestDispatcher("tela.jsp").forward(request, response);

    }

}
