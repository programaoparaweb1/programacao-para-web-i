/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Guilherme
 */
@WebServlet(name = "LogoutServlet", urlPatterns = {"/logout"})
public class LogoutServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        Cookie ck = new Cookie("identifica", "");
        Cookie cookie2 = new Cookie("id", "");
        ck.setMaxAge(0);
        cookie2.setMaxAge(0);
        response.addCookie(ck);
        response.addCookie(cookie2);

        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }

        out.print("you are successfully logged out!");

        request.getRequestDispatcher("index.jsp").include(request, response);
    }
}
