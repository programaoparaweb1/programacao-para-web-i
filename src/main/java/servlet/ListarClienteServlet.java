package servlet;

import controle.ClienteDAO;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Cliente;

/**
 *
 * @author Guilherme
 */
@WebServlet(name = "ListarCliente", urlPatterns = {"/ListarCliente"})
public class ListarClienteServlet extends HttpServlet {

    private final String url = "jdbc:mysql://yonathan.com.br/prog-web-1";
    private final String login = "faculdades";
    private final String passwd = "FbZRwUZDBXNZP7Ty";
    private Connection con;
    private ClienteDAO cdao;

    @Override
    public void init(ServletConfig config) throws ServletException {
        try {
            super.init(config);
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, login, passwd);
        } catch (ClassNotFoundException | SQLException ex) {

        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        init(this);
        try {

//        Connection con = DriverManager.getConnection(url, master, master);
         cdao = new ClienteDAO();
        /*
             Object inicializar = cdao.inicializar();
             ClienteDAO cd = (ClienteDAO) inicializar;
             ArrayList<Cliente> retorno = cd.getLista();
             */
            List lista = cdao.ler(con);
            ArrayList<Cliente> retorno = new ArrayList<>();
            retorno.addAll(lista);

            //String nome = clienteEe.getNome();
            //colocando a string no request
            request.setAttribute("resultado", retorno);

            response.setContentType("text/html;charset=UTF-8");
            //enviando o resultado para uma outra pagina
            request.getRequestDispatcher("listarcliente.jsp").forward(request, response);
            
        } catch (SQLException ex) {
            //Logger.getLogger(ClienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
