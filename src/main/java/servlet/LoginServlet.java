package servlet;

import controle.ClienteDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.Cliente;

/**
 *
 * @author 0230090
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
    
     private final String url = "jdbc:mysql://yonathan.com.br/prog-web-1";
    private final String login = "faculdades";
    private final String passwd = "FbZRwUZDBXNZP7Ty";
    private Connection con;
    private ClienteDAO cdao;

    @Override
    public void init(ServletConfig config) throws ServletException {
        try {
            super.init(config);

            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, login, passwd);
        } catch (ClassNotFoundException | SQLException ex) {

        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {        
        response.sendRedirect("erro.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
             init(this);
         
        try {
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();

            String loginn = request.getParameter("nome");
            String senha = request.getParameter("pass");
            
            Cliente cl1=new Cliente();
            cl1.setEmail(loginn);
            cl1.setSenha(senha);
            
            
            
            cdao = new ClienteDAO();
            Cliente resp = cdao.pesquisar(con, cl1);    
     
            
            if (resp != null) {                
//                Cliente cl2=(Cliente)resp;
                
                Cookie cookie = new Cookie("identifica", resp.getNome());
                Long i=resp.getCodCliente();
                Cookie cookie2 = new Cookie("id", i.toString() );
                response.addCookie(cookie);
                response.addCookie(cookie2);
                 
                HttpSession session = request.getSession(true);
                session.setAttribute("identifica", resp.getNome());
                session.setAttribute("email", resp.getEmail());
                session.setAttribute("id", resp.getCodCliente());

                //response.sendRedirect("index.jsp");
                out.println("Bem vindo " + resp.getNome() + "! Agora você está logado !");
                
                RequestDispatcher rd = request.getRequestDispatcher("tela.jsp");
                rd.include(request, response);
                
            } else {
               
                String msg = "<h1> Ocorreu um erro durante o processo de autenticação </h1>";       
                request.setAttribute("resultado", msg);
                response.setContentType("text/html;charset=UTF-8");
                request.getRequestDispatcher("erro.jsp").forward(request, response);

            }
           
      
         }catch (Exception e){
            
        }
    }

}
