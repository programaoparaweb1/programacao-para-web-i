package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author 0230090
 */
@WebServlet(name = "ProcessoServlet", urlPatterns = {"/processo"})
public class ProcessoServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String pag = request.getParameter("pagamento");
        String texto="";
        String url="erro.jsp";
        if(pag.equals("cc")){
            texto="cartão";
            url="cartaopasse.jsp";
        }
        else if(pag.equals("pp")){
            texto="paypal";
            url="paypalpasse.jsp";
        }
        else {
            texto="erro";
        }
        
        request.setAttribute("resultado", texto);       
       // request.getRequestDispatcher("finalizar.jsp").forward(request, response);
        request.getRequestDispatcher(url).forward(request, response);
        

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String textA = request.getParameter("file");
        String textB = request.getParameter("valor");

        request.setAttribute("resultado", textA);
        request.setAttribute("resultado", textB);       

        
       // response.setContentType("text/html;charset=UTF-8");       
        request.getRequestDispatcher("pagamento.jsp").forward(request, response);

    }

}
