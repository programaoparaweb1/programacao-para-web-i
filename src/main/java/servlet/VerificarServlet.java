package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author 0230090
 */
@WebServlet(name = "VerificarServlet", urlPatterns = {"/verificar"})
public class VerificarServlet extends HttpServlet {

   
     @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {        
        response.sendRedirect("erro.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
         String vTipo = request.getParameter("tipo");
         switch (vTipo) {
             case "paypal":
                 {
                     String vPasse = request.getParameter("pass");
                     break;
                 }
             case "cartao":
                 {
                     String vPasse = request.getParameter("pass");
                     break;
                 }
             default:
                 //something went wrong!
                 request.setAttribute("resultado", "Erro no pagamento.");
                 request.getRequestDispatcher("erro.jsp").forward(request, response);
                 break;
         }
        
        //request.setAttribute("resultado", vTipo);
        //resposta.jsp
        request.getRequestDispatcher("finalizar.jsp").forward(request, response);
        
       
    }
    
}
