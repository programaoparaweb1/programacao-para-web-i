package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author 0230090
 */
@WebServlet(name = "FinalizarServlet", urlPatterns = {"/finalizar"})
public class FinalizarServlet extends HttpServlet {

 
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String conf = request.getParameter("confirmar");
        String texto="";
        if(conf.equals("sim")){
           texto="sim";
        }
        else{
           texto="nao";
           request.getRequestDispatcher("tela.jsp").forward(request, response);
        }
        request.setAttribute("resultado", texto);
        request.getRequestDispatcher("final.jsp").forward(request, response);
        
    }

  
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       // processRequest(request, response);
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

   
}
