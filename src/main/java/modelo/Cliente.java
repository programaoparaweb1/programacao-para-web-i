package modelo;

import java.util.Date;

/**
 *
 * @author Guilherme
 */
public class Cliente {

    private long CodCliente;
    private String nome;
    private String email;
    private String senha;

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    
    public long getCodCliente() {
        return CodCliente;
    }

    public void setCodCliente(long CodCliente) {
        this.CodCliente = CodCliente;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Cliente{" + "CodCliente=" + CodCliente + ", nome=" + nome + ", "
                + "email=" + email + ", senha=" + senha + '}';
    }

    
    
}
