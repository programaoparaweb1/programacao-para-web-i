package modelo;

//import com.paypal.api.payments.*;
//import com.paypal.base.rest.OAuthTokenCredential;
//import com.paypal.base.rest.PayPalRESTException;

        

/**
 *
 * @author Guilherme
 */

        
public class Paypal implements Tipo{
//    private OAuthTokenCredential tokenCredential;
//    private String tokenCliente;
    private int idCliente;
    private String usuario;
    private int idPaypal;
    private String senha;
    private double saldo;
  
//    public Paypal() throws PayPalRESTException {
//        try{
//            this.tokenCredential = Payment.initConfig(new File("../sdk_config.properties"));
//        }catch (PayPalRESTException e){
//            this.tokenCredential = null;
//        }
//    }
    
    public int getIdPaypal() {
        return idPaypal;
    }

    public void setIdPaypal(int idPaypal) {
        this.idPaypal = idPaypal;
    }
    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
//     public String getTokenCliente(){
//         return this.tokenCliente;
//     }
//    public void setTokenCliente() throws PayPalRESTException{
//        try{
//            this.tokenCliente = new OAuthTokenCredential(this.getUsuario(), this.getSenha()).getAccessToken();
//        }catch(PayPalRESTException e){
//            this.tokenCliente = "";
//        }
//    
//    }
    
    
}
