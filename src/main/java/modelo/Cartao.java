package modelo;

import java.util.Date;

/**
 *
 * @author Guilherme
 */
public class Cartao implements Tipo{

    private String nome;
    private int numero;
    private Date vencimento;
    private String bandeira;
    private long id_usuario;
    private long id_cartao ;

    public long getId_cartao() {
        return id_cartao;
    }

    public void setId_cartao(long id_cartao) {
        this.id_cartao = id_cartao;
    }
    

    public long getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(long id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public String getBandeira() {
        return bandeira;
    }

    public void setBandeira(String bandeira) {
        this.bandeira = bandeira;
    }

    @Override
    public String toString() {
        return "Cartao{" + "nome=" + nome + ", numero=" + numero + ", vencimento=" + 
                vencimento + ", bandeira=" + bandeira + '}';
    }
    
    
    
}
