

package controle;

import java.sql.Connection; 
import java.sql.SQLException; 
import java.util.List; 
  
/** 
 * 
 * @author 0230090 
 * @param <T> 
 */
public interface InterfaceBanco<T> { 
  
    public void criar(Connection con, T obj) throws SQLException; 
  
    public List<T> ler(Connection con) throws SQLException; 
      
    public  void alterar(Connection con, T obj) throws SQLException; 
      
     public T pesquisar(Connection con, T obj) throws SQLException;
   
       
     public  void excluir(Connection con, T obj) throws SQLException ; 
  
} 