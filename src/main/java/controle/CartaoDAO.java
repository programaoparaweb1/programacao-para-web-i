package controle;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelo.Cartao;

/**
 *
 * @author Guilherme
 */
public class CartaoDAO implements InterfaceBanco<Cartao> {

    @Override
    public void criar(Connection con, Cartao obj) throws SQLException {
        if ((obj instanceof Cartao)) {
            Cartao cc = (Cartao) obj;
            String sql = "INSERT INTO Cartao (id_usuario, nome, numero, bandeira, vencimento)"
                    + "VALUES (?, ?, ?, ?, ?)";

            try (PreparedStatement stm = con.prepareStatement(sql)) {
                stm.setLong(1, cc.getId_usuario());
                stm.setString(2, cc.getNome());
                stm.setLong(3, cc.getNumero());
                stm.setString(4, cc.getBandeira());
                stm.setDate(5, (Date) cc.getVencimento());

                stm.executeUpdate();
                System.out.println("Sucesso");

            } catch (Exception e) {
                e.printStackTrace();

                System.out.println("Inclusão mal sucedida");
            }
        } else {
            System.out.println("Objeto não Cliente");
        }
    }

    @Override
    public List<Cartao> ler(Connection con) throws SQLException {
        List<Cartao> lista = new ArrayList<>();
        String sql = "select * from Cartao ";
        try (PreparedStatement stm = con.prepareStatement(sql);
                ResultSet rs = stm.executeQuery()) {
            while (rs.next()) {

                Cartao cc = new Cartao();
                cc.setId_cartao(Long.parseLong(rs.getString(1)));
                cc.setId_usuario(Long.parseLong(rs.getString(2)));
                cc.setNome(rs.getString(3));
                cc.setNumero(Integer.parseInt(rs.getString(4)));
                cc.setBandeira(rs.getString(5));                
                cc.setVencimento(null);
                

                lista.add(cc);

            }

        }
        return lista;
    }

    @Override
    public void alterar(Connection con, Cartao obj) throws SQLException {
        Cartao o = pesquisar(con, obj);

        if (o != null) {
            Cartao cl = (Cartao) obj;
            String nome = "'" + cl.getNome() + "'";
            String sql = "UPDATE  Cartao"
                    + " SET nome='" + cl.getNome()
                    + "', id_cartao=" + cl.getId_cartao()
                    + "', id_usuario =" + cl.getId_usuario()
                    + "', bandeira  =" + cl.getBandeira()
                    + "', vencimento =" + cl.getVencimento()
                    + "WHERE id_cartao = "+ cl.getId_cartao();

            System.out.println(sql);
            try (PreparedStatement stm = con.prepareStatement(sql)) {
                stm.executeUpdate();
                System.out.println("Cliente Alterado com Sucesso");

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Inclusão mal sucedida");
            }
        } else {
            System.out.println("Não Encontrado");
        }
    }

    @Override
    public Cartao pesquisar(Connection con, Cartao obj) throws SQLException {
        Cartao cc = null;
        String SQL = "SELECT * FROM Cartao WHERE id_cartao ='" + obj.getNome() + "'";
        PreparedStatement st = con.prepareStatement(SQL);
        ResultSet rs = st.executeQuery();
        while (rs.next()) {            

                cc = new Cartao();
                cc.setId_cartao(Long.parseLong(rs.getString(1)));
                cc.setId_usuario(Long.parseLong(rs.getString(2)));
                cc.setNome(rs.getString(3));
                cc.setNumero(Integer.parseInt(rs.getString(4)));
                cc.setBandeira(rs.getString(5));                
                cc.setVencimento(null);
        }
        return cc;
    }

    @Override
    public void excluir(Connection con, Cartao obj) throws SQLException {
        //String std = (String) obj;
        if (obj != null) {
            String sql = "DELETE FROM Cartao  WHERE id_cartao=" + obj.getId_cartao();
            try (PreparedStatement stm = con.prepareStatement(sql)) {
                stm.executeUpdate();
                System.out.println("Cartao Excluido com Sucesso");
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Inclusão mal sucedida");
            }
        } else {
            System.out.println("Não Encontrado");
        }
    }
}
