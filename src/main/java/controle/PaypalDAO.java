
package controle;

//import com.paypal.base.rest.PayPalRESTException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Cliente;
import modelo.Paypal;

/**
 *
 * @author 0335045
 */
public class PaypalDAO implements InterfaceBanco {

    @Override
    public void criar(Connection con, Object obj) throws SQLException {
        Paypal conta = (Paypal) obj;
        String SQL = "SELECT * FROM paypal WHERE usuario=" + conta.getUsuario();
        try (Statement st = con.createStatement()) {
            ResultSet res = st.executeQuery(SQL);

            res.moveToInsertRow();

            res.updateString("usuario", conta.getUsuario());
            res.updateString("senha", conta.getSenha());
            res.updateInt("id_usuario", conta.getIdCliente());

            res.insertRow();

            res.close();
        }
        con.close();
    }

    public ArrayList<Paypal> lerPaypal(Connection con) throws SQLException{ //, PayPalRESTException {
        ArrayList<Paypal> contas = new ArrayList<Paypal>() {
        };

        String SQL = "SELECT * FROM paypal";
        Statement st = con.createStatement();

        ResultSet res = st.executeQuery(SQL);
        while (res.next()) {
            Paypal conta = new Paypal();
            conta.setIdPaypal(res.getInt("id_paypal"));
            conta.setUsuario(res.getString("usuario"));
            conta.setSenha(res.getString("senha"));
            conta.setIdCliente(res.getInt("id_usuario"));
            contas.add(conta);
        }

        return contas;
    }

    @Override
    public void alterar(Connection con, Object obj) throws SQLException {
        Paypal conta = (Paypal) obj;
        String SQL = "SELECT * FROM paypal WHERE id_paypal=" + conta.getIdPaypal();
        Statement st = con.createStatement();
        try (ResultSet res = st.executeQuery(SQL)) {
            res.moveToCurrentRow();

            res.updateString("usuario", conta.getUsuario());
            res.updateString("senha", conta.getSenha());
            res.updateInt("id_usuario", conta.getIdCliente());

            res.insertRow();

            res.close();
        }
        st.close();
        con.close();

    }

    @Override
    public Object pesquisar(Connection con, Object obj) throws SQLException {
        Cliente cli = (Cliente) obj;
        Paypal conta = new Paypal();
        String SQL = "SELECT * FROM paypal WHERE id_usuario=" + cli.getCodCliente();
        Statement st = con.createStatement();

        ResultSet res = st.executeQuery(SQL);
        while (res.next()) {
            conta.setIdPaypal(res.getInt("id_paypal"));
            conta.setUsuario(res.getString("usuario"));
            conta.setSenha(res.getString("senha"));
            conta.setIdCliente(res.getInt("id_usuario"));
        }
        return conta;
    }

    @Override
    public void excluir(Connection con, Object obj) throws SQLException {
        Paypal conta = (Paypal) obj;
        String SQL = "SELECT * FROM paypal WHERE id_paypal=" + conta.getIdPaypal();
        try (Statement st = con.createStatement()) {
            ResultSet res = st.executeQuery(SQL);
            
            res.moveToCurrentRow();
            
            res.deleteRow();
            
            res.close();
        }
        con.close();

    }

    @Override
    public List<Object> ler(Connection con) throws SQLException {
        try {
            List<Paypal> lista = this.lerPaypal(con);
            ArrayList<Object> objs = new ArrayList<>();
            for (Paypal pp : lista) {
                objs.add((Object) pp);
            }
            return objs;
        } catch (Exception ex) {
            Logger.getLogger(PaypalDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
