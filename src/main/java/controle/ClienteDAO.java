package controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelo.Cliente;

/**
 * Classe que ClienteDAO implementa interface OperacoesDAO
 *
 * @author Guilherme
 */
public class ClienteDAO implements InterfaceBanco<Cliente> {

    @Override
    public void criar(Connection con, Cliente obj) throws SQLException {

        if ((obj instanceof Cliente)) {
            Cliente cliente = (Cliente) obj;
            String sql = "INSERT INTO Usuario (nome, senha, email) "
                    + "VALUES (?, ?, ?)";

            try (PreparedStatement stm = con.prepareStatement(sql)) {
                stm.setString(1, cliente.getNome());
                stm.setString(2, cliente.getSenha());
                stm.setString(3, cliente.getEmail());
                stm.executeUpdate();
                System.out.println("Sucesso");

            } catch (Exception e) {
                e.printStackTrace();

                System.out.println("Inclusão mal sucedida");
            }
        } else {
            System.out.println("Objeto não Cliente");
        }
    }

    @Override
    public List<Cliente> ler(Connection con) throws SQLException {
        List<Cliente> lista = new ArrayList<>();
        String sql = "select * from Usuario";
        try (PreparedStatement stm = con.prepareStatement(sql);
                ResultSet rs = stm.executeQuery()) {
            while (rs.next()) {

                Cliente cliente = new Cliente();
                cliente.setNome(rs.getString(2));
                cliente.setEmail(rs.getString(4));
                cliente.setSenha(rs.getString(3));
                cliente.setCodCliente(Long.parseLong(rs.getString(1)));
                lista.add(cliente);

            }

        }
        return lista;
    }

    @Override
    public void alterar(Connection con, Cliente obj) throws SQLException {
        Cliente o = pesquisar(con, obj);

        if (o != null) {
            Cliente cl = (Cliente) obj;
            String nome = "'" + cl.getNome() + "'";
            String sql = "UPDATE  Usuario SET nome='" + cl.getNome()
                    + "', senha=" + cl.getSenha()
                    + ", email=" + cl.getEmail()
                    + "' WHERE id_usuario=" + cl.getCodCliente();

            System.out.println(sql);
            try (PreparedStatement stm = con.prepareStatement(sql)) {
                stm.executeUpdate();
                System.out.println("Cliente Alterado com Sucesso");

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Inclusão mal sucedida");
            }
        } else {
            System.out.println("Não Encontrado");
        }
    }

    @Override
    public Cliente pesquisar(Connection con, Cliente obj) throws SQLException {
        Cliente   user = null;
        String SQL = "SELECT * FROM Usuario WHERE email='" + obj.getEmail()+"'";
        PreparedStatement st = con.prepareStatement(SQL);
        ResultSet rs = st.executeQuery();
        while (rs.next()) {
            user = new Cliente();
                    user.setNome(rs.getString(2));
                    user.setSenha(rs.getString(3));
                    user.setEmail(rs.getString(4));                    
                    user.setCodCliente(Long.parseLong(rs.getString(1)));          
            
        }
        return user;
    }

        @Override
        public void excluir(Connection con, Cliente obj) throws SQLException {
            //String std = (String) obj;
            if (obj != null) {
                String sql = "DELETE FROM Usuario  WHERE email=" + obj.getEmail();
                try (PreparedStatement stm = con.prepareStatement(sql)) {
                    stm.executeUpdate();
                    System.out.println("Cliente Excluido com Sucesso");
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Inclusão mal sucedida");
                }
            } else {
                System.out.println("Não Encontrado");
            }
        }

    }
