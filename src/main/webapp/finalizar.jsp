<%-- 
    Document   : finalizar
    Created on : 25/11/2015, 09:36:23
    Author     : 0230090
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Confirmar Pagamento</title>
    </head>
    <body>
        <h1>Confirmar Pagamento!</h1>
        <img src="barras.jpg" alt="logo"  height="210">
        <br/><br/>
         <table border="1" style="width:100%">
            <tr>
                <th>Forma de Pagamento</th>
                              
            </tr>
            <tr>
                <td>${resultado}</td>
                	                
            </tr>           
        </table>
                <br/>
        <table border="1" style="width:100%">
            <tr>
                <th>Nome Da Empresa:</th>
                <th>Valor:</th>	                
            </tr>
            <tr>
                <td>Eve</td>
                <td>R$ 1.99</td>	                
            </tr>           
        </table>
        <br/>
        <table border="1" style="width:100%">
            <tr>
                <th>Dados do comprador:</th>

            </tr>
            <tr>
                <td>Nome</td>
                <td>Guilherme</td>	                
            </tr>           
            <tr>
                <td>Endereço</td>
                <td>Rua das Flores, 44</td>	                
            </tr>    
            <tr>
                <td>Telefone</td>
                <td>3366</td>	                
            </tr>
            <tr>
                <td>Email</td>
                <td>gui@com</td>	                
            </tr>
        </table>
        <br/>

        <form action="finalizar" method="POST">
            <h2>Confirmar Pagamento?</h2>
                            
            <input type="radio" name="confirmar" value="sim" /> Sim <br/>
            <input type="radio" name="confirmar" value="no" checked/> Não <br/>
            <input type="submit" value="Enviar" >

        </form>

    </body>
</html>
