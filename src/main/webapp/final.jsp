<%-- 
    Document   : final
    Created on : 08/12/2015, 22:47:05
    Author     : Guilherme
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
         <style>
            table {
                border-spacing: 15px;
            }
            table, th, td {
                border: 1px solid black;
                /*border-collapse: collapse;*/

            }
            th, td {
                padding: 5px;
            }
        </style>
    </head>
    <body>
        <fieldset>
            <legend> <h1> Seu Pedido </h1> </legend>
            <table style="width:100%">
                <tr>
                    <th>Produto</th>
                    <th>Preço</th>
                    <th>Quantidade</th>
                    <th>Total</th>
                </tr>
                
                <tr>
                    <td> PayPal</td>
                    <td>R$ 50 </td>
                    <td> X </td>
                    <td>R$ 100 </td>
                </tr>
                
                <tr>
                    <th> </th>
                    <th> </th>
                    <th>Frete</th>
                    <th>Total</th>
                </tr>
                <tr>
                    <td>${resultado} </td>
                    <td> </td>
                    <td>R$ 50</td>
                    <td>R$ 150</td>
                </tr>
            </table>
        </fieldset>

     
    </body>  
   
</html>
